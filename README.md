> This project is just a mirror of [https://oisd.nl/](https://oisd.nl/)

**Gitlab [mirror](https://gitlab.com/ookangzheng/dbl-oisd-nl/-/tree/master)**
**Github [mirror](https://github.com/ookangzheng/dbl-oisd-nl)**

```bash
# Lists that are skipped: https://dbl.oisd.nl/notinuse.html
# Included blocklists: https://includes.oisd.nl

    __ __ ____ ____ 
   /  (  / ___(    \
  (  O )(\___ \) D (
   \__(__(____(____/
   b l o c k l i s t
   b y   s j h g v r
   since august 2018

Designed to be set-it and forget-it
Use it, love it

```

## False positives 🔥

[Report on reddit](https://www.reddit.com/r/oisd_blocklist/) || [Report on site](https://oisd.nl/?p=fp)

## Blocks

Ads, (Mobile) App Ads, Phishing, Malvertising, Malware, Spyware, Ransomware, CryptoJacking, Fraud, Scam
... Telemetry, Analytics, Tracking (Where not needed for proper functionality)

##

Torrent, Warez, Porn, Crypto Exchanges, News Satire, Slickdeals (or shopping sites in general), Google (shopping), Facebook, Twitter, Snapchat, Link Shortners, Affiliate/Tracking Links, etc.

It basically shouldn't interfere with anything you don't want it interfering with :)

## Download

### Abp rule (Adblocker syntax)

abp: [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/abp.txt) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/abp.txt)

abp_light: [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/abp_light.txt) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/abp_light.txt)

### Just domains

domains: [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/dbl.txt) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/dbl.txt)

domains_light: [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/dbl_light.txt) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/dbl_light.txt)

### Just domains with Wildcards

domains wildcards: [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/dblw.txt) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/dblw.txt)

domains wildcards_light: [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/dblw_light.txt) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/dblw_light.txt)

### Hosts file

hosts : [Github](https://github.com/ookangzheng/dbl-oisd-nl/blob/master/hosts.txt?raw=true) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/-/raw/master/hosts.txt)

hosts_light : [Github](https://github.com/ookangzheng/dbl-oisd-nl/blob/master/hosts_light.txt?raw=true) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/-/raw/master/hosts_light.txt)

### DNSMasq

dnsmasq : [Github](https://github.com/ookangzheng/dbl-oisd-nl/blob/master/dnsmasq.txt?raw=true) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/-/raw/master/dnsmasq.txt)

dnsmasq_light : [Github](https://github.com/ookangzheng/dbl-oisd-nl/blob/master/dnsmasq_light.txt?raw=true) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/-/raw/master/dnsmasq_light.txt)

### PAC

rpz : [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/rpz.rpz) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/rpz.rpz)

rpz_basic : [Github](https://raw.githubusercontent.com/ookangzheng/dbl-oisd-nl/master/rpz_basic.rpz) | [Gitlab](https://gitlab.com/ookangzheng/dbl-oisd-nl/raw/master/rpz_basic.rpz)
